# boost-pottcpp

Presentation slides for a C++ user group meeting about boost::any and
boost::string_ref libraries.


## License

reveal.js is MIT licensed

Copyright (C) 2015 Hakim El Hattab, http://hakim.se
